# Forum involvment exercise

This guide will walk you through to set up an openedx exercise to check a user forum involvment.

1. Upload `get_edx_username.html` to the course "Files and uploads"
2. Create a blank advanced problem
3. Copy the following code (change `FORUM_ROOT_URL`, `ADMIN_USER` and `API_KEY`)
    ```xml
    <problem>
        <p>This problem check your forum involvment</p>

        <script type="loncapa/python">
        import requests
        def check_discourse(expect, ans):
        api_user = 'ADMIN_USER'
        api_key = 'API_KEY'
        forum_url = 'FORUM_ROOT_URL'
        user_profile_url =  forum_url + '/u/' + ans +".json"
        
        try:
            rsp = requests.request("GET", user_profile_url, headers={'Api-Username': api_user, 'Api-Key': api_key})

            if rsp.status_code == 200:
                user_data = rsp.json()
                msg_count = str(user_data['user']['post_count'])
                msg = 'Your name is ' + ans + ' and you made '+ msg_count + ' posts'
                ok = True
                grade = 1.0
            else:
                msg = "You don't have an account"
                ok = False
                grade = 0.0
        except requests.exceptions.RequestException as e:
            msg = "Something went wrong please try again later."
            ok = False
            grade = 0.0
            
        return {
            'input_list': [
                { 'ok': ok, 'msg': msg, 'grade_decimal': grade},
            ]
        }
        </script>
        <customresponse cfn="check_discourse">
            <jsinput gradefn="gradefn"
            height="0" width="0"
            html_file="/static/get_edx_username.html"
            title="Get Edx Username"/>
        </customresponse>
    </problem>
    ```

## Discourse user data 

`$FORUM_ROOT_URL/u/$USERNAME.json` and `$FORUM_ROOT_URL/u/$USERNAME/summary.json` are the most relevant urls to get user data (`topic_count`, `post_count`, `like_received`, etc) but any Discourse page/topic/category/etc have the correspondant `.json` retrievable data so feel free to adapt your exercice to your needs.
